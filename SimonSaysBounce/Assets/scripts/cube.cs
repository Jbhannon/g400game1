﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cube: MonoBehaviour {

    public float moveSpeed;
    private bool onGround;
    public float jumpPressure;
    private float minJump;
    private float maxJumpPressure;
    private Rigidbody rbody;
    private Animator anim;
    public float slowSpeed;
    public Slider healthbar;

	// Use this for initialization
	void Start () {
        if (moveSpeed == 0) {
            moveSpeed = 1;
        }
        onGround = true;
        jumpPressure = 0f;
        minJump = 2f;
        maxJumpPressure = 10f;
        rbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        healthbar.value = jumpPressure;
    }

    // Update is called once per frame
    void Update () {

        if (onGround)
        {

            if (Input.GetButton("Jump"))
            {
                transform.Translate((moveSpeed / slowSpeed) * Input.GetAxis("Horizontal") * Time.deltaTime, 0f, (moveSpeed / slowSpeed) * Input.GetAxis("Vertical")* Time.deltaTime);
                if (jumpPressure < maxJumpPressure)
                {
                    jumpPressure += Time.deltaTime * 5f;
                }
                else
                {
                    jumpPressure = maxJumpPressure;
                }
                anim.SetFloat("jumpPressure", jumpPressure + minJump);
                anim.speed = 1f + (jumpPressure/10f);
            }

            else
            {
                transform.Translate(moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime, 0f, moveSpeed * Input.GetAxis("Vertical") * Time.deltaTime);

                if (jumpPressure > 0f)
                {
                    jumpPressure = jumpPressure + minJump;
                    rbody.velocity = new Vector3(0f, jumpPressure *2f, 0f);
                    jumpPressure = 0f;
                    onGround = false;
                    anim.SetFloat("jumpPressure", 0f);
                    anim.SetBool("onGround", onGround);
                    anim.speed = 1f;
                }

            }

        }

        else {
            transform.Translate((moveSpeed / slowSpeed) * Input.GetAxis("Horizontal") * Time.deltaTime, 0f, (moveSpeed / slowSpeed) * Input.GetAxis("Vertical") * Time.deltaTime);
        }

        healthbar.value = jumpPressure;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            onGround = true;
            anim.SetBool("onGround", onGround);
        }
    }


}
