﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {

    public GameObject player;       //Public variable to store a reference to the player game object


    private Vector3 offset;         //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start () {
        offset = transform.position - player.transform.position;
		offset.y += (transform.position.y / 2f);
		offset.z -= (transform.position.y / 2f);
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = player.transform.position + offset;
        transform.rotation = Quaternion.Euler(35f + (transform.position.y / 1.5f), 0f, 0f);
    }
}
